'use strict';

module.exports = {
	theme: {
		slug: 'cafetos',
		name: 'Los cafetos',
		author: 'David Chay Díaz'
	},
	dev: {
		browserSync: {
			live: true,
			proxyURL: 'localhost/wp',
			bypassPort: '8181'
		},
		browserslist: [ // See https://github.com/browserslist/browserslist
			'> 1%',
			'last 2 versions'
		],
		debug: {
			styles: true, // Render verbose CSS for debugging.
			scripts: true // Render verbose JS for debugging.
		}
	},
	export: {
		compress: true
	}
};
