<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wprig
 */

?>

<footer id="colophon" class="site-footer">
	<div class="site-branding">
		<div class="site-logo">
		<?php the_custom_logo(); ?>
		</div>
		<p class="site-title"><?php bloginfo( 'name' ); ?></p>
		<span class="site-subtitle">Residencial</span>
		
	</div><!-- .site-branding -->
	<div class="site-info">
		<small>Es un desarrollo de</small>
		<h4>Servicio de maquinaria de concreto S.A. de C.V.</h4>
		<span>&copy;<?php echo date("Y"); ?>. Todos los derechos reservados.</span>
	</div><!-- .site-info -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
