<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wprig
 */

?>

<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php the_content(); ?>
</section><!-- #post-<?php the_ID(); ?> -->
