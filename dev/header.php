<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wprig
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php if ( ! wprig_is_amp() ) : ?>
		<script>document.documentElement.classList.remove("no-js");</script>
	<?php endif; ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'wprig' ); ?></a>

	<?php if( is_front_page() ) : 
		$header_image_url = get_the_post_thumbnail_url(get_the_ID(),'full'); ?>
	<header id="masthead" class="site-header front-header-image" style="background-image: url(<?php echo $header_image_url; ?>);">
	<?php elseif ( has_header_image() ) : 
		$header_image_url = get_header_image(); ?>
	<header id="masthead" class="site-header header-image" style="background-image: url(<?php echo $header_image_url; ?>);">
	<?php else : ?>
	<header id="masthead" class="site-header" >
	<?php endif; ?>
		<div class="header-overlay">

			<div class="header-topbar">
				<div class="topbar-left">
				<?php if ( is_active_sidebar( 'topbar-left' ) ) : ?>
						<div id="topbarleft" class="topbar-sidebar-left widget-area" role="complementary">
							<?php dynamic_sidebar( 'topbar-left' ); ?>
						</div><!-- #topbar-left-sidebar -->
					<?php endif; ?>
				</div>
				<div class="topbar-right">
					<?php if ( is_active_sidebar( 'topbar-right' ) ) : ?>
						<div id="topbarright" class="topbar-sidebar-irght widget-area" role="complementary">
							<?php dynamic_sidebar( 'topbar-right' ); ?>
						</div><!-- #topbar-right-sidebar -->
					<?php endif; ?>
				</div>
			</div>

			<nav id="site-navigation" class="main-navigation" aria-label="<?php esc_attr_e( 'Main menu', 'wprig' ); ?>"
					<?php if ( wprig_is_amp() ) : ?>
						[class]=" siteNavigationMenu.expanded ? 'main-navigation toggled-on' : 'main-navigation' "
					<?php endif; ?>
				> <!-- #abreetiqueta nav  -->
					<?php if ( wprig_is_amp() ) : ?>
						<amp-state id="siteNavigationMenu">
							<script type="application/json">
								{
									"expanded": false
								}
							</script>
						</amp-state>
					<?php endif; ?>

					<button class="menu-toggle" aria-label="<?php esc_attr_e( 'Open menu', 'wprig' ); ?>" aria-controls="primary-menu" aria-expanded="false"
						<?php if ( wprig_is_amp() ) : ?>
							on="tap:AMP.setState( { siteNavigationMenu: { expanded: ! siteNavigationMenu.expanded } } )"
							[aria-expanded]="siteNavigationMenu.expanded ? 'true' : 'false'"
						<?php endif; ?>
					>
						<?php esc_html_e( 'Menu', 'wprig' ); ?>
					</button>

					<div class="primary-menu-container">
						<?php

						wp_nav_menu(
							array(
								'theme_location' => 'primary',
								'menu_id'        => 'primary-menu',
								'container'      => 'ul',
							)
						);

						?>
					</div>
			</nav>

			<div class="site-branding">
				<?php the_custom_logo(); ?>
				<?php if ( is_front_page() && is_home() ) : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php else : ?>
					<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php endif; ?>
				<span class="site-subtitle">Residencial</span>
				<?php $wprig_description = get_bloginfo( 'description', 'display' ); ?>
				<?php if ( $wprig_description || is_customize_preview() ) : ?>
					<p class="site-description"><?php echo $wprig_description; /* WPCS: xss ok. */ ?></p>
				<?php endif; ?>
			</div><!-- .site-branding -->

			<?php if( is_front_page() ): ?>
				<div class="call-to-action-header">
					<p>Empieza a vivir tu sueño, Pide una cotización</p>
					<button type="button" class="call-to-action-header-button">Contactanos</button>
					<button type="button" class="call-to-action-header-button-alt">962 150 15 00</button>
				</div>
			<?php endif; ?>

		</div> <!-- .header-overlay -->
	</header><!-- #masthead -->
